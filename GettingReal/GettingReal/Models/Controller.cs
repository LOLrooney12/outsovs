﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GettingReal.Models
{
    [Serializable]
    public class Controller
    {
        public VentilationAggregate SelectedVentilationAggregate { get; set; }
        public Customer SelectedCustomer { get; set; }
        public Building SelectedBuilding { get; set; }
        public Room SelectedRoom { get; set; }
        public Floor SelectedFloor { get; set; }

        private VentilationAggregateRepository _ventilationAggregateRepository;
        private CustomerRepository _customerRepository;
        private BuildingRepository _buildingRepository;
        private RoomRepository _roomRepository;
        private FloorRepository _floorRepository;

        public List<VentilationAggregate> AllVentilationAggregates { get; private set; }
        public List<Building> AllBuildings { get; private set; }
        public List<Room> AllRooms { get; private set; }
        public List<Customer> AllCustomers { get; private set; }
        public List<Floor> AllFloors { get; private set; }

        public Controller()
        {
            _ventilationAggregateRepository = new VentilationAggregateRepository();
            _customerRepository = new CustomerRepository();
            _buildingRepository = new BuildingRepository();
            _roomRepository = new RoomRepository();
            _floorRepository = new FloorRepository();

            AllCustomers = _customerRepository.GetAllCustomers();
            AllBuildings = _buildingRepository.GetAllBuildings();
            AllVentilationAggregates = _ventilationAggregateRepository.GetAllVentilationAggregates();
            AllRooms = _roomRepository.GetAllRooms();
            AllFloors = _floorRepository.GetAllFloors();
        }

        public Customer AddCustomer(string name, string building)
        {

            SelectedCustomer = new Customer(name, building);

            return SelectedCustomer;
        }

        public Building AddBuilding(string name)
        {
            SelectedBuilding = new Building(name);

            _buildingRepository.AddBuilding(SelectedBuilding);

            if (SelectedCustomer != null)
            {
                SelectedCustomer.AddBuilding(SelectedBuilding);
            }


            return SelectedBuilding;
        }

        public VentilationAggregate AddVentilationsAggregate(string fileName, string orderNumber)
        {
            SelectedVentilationAggregate = new VentilationAggregate(fileName, orderNumber);

            if (SelectedVentilationAggregate != null)
            {
                _ventilationAggregateRepository.AddVentilationAggregate(SelectedVentilationAggregate);
            }

            if (SelectedVentilationAggregate != null && SelectedBuilding != null)
            {
                SelectedBuilding.AddVentilationAggregate(SelectedVentilationAggregate);
            }

            return SelectedVentilationAggregate;

        }
    }
}
